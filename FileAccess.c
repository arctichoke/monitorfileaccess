#include "FileAccess.h"
#include <pthread.h>

/**
 * @brief: Starts access to file.
 * @param[in] pFileAccess pointer to FileAccess.
 * @param[in] id of the process.
 */
void startAccess(FileAccessPtr pFileAccess, long int id)
{
    pthread_mutex_lock(&(pFileAccess->mutex));      // Lock the mutex
    while (pFileAccess->sum + id > pFileAccess->max) { // Check to see if the sum of id is less than max.
        pthread_cond_wait(&(pFileAccess->waitForAccess), &(pFileAccess->mutex)); // Wait for conditional variable
    }
    pFileAccess->sum += id;                 // thread owns the mutex and the condition is true.
    pthread_mutex_unlock(&(pFileAccess->mutex)); // unlock the mutex since thread has access.
}

/**
 * @brief: Ends access to the file.
 * @param[in] pFileAccess pointer to FileAccess.
 * @param[in] id of the process.
 */
void endAccess(FileAccessPtr pFileAccess, long int id)
{
    pthread_mutex_lock(&(pFileAccess->mutex));
    pFileAccess->sum  -= id;                    // decrement the id from the sum
    pthread_cond_broadcast(&(pFileAccess->waitForAccess)); // signal other process waiting on the condition.
    pthread_mutex_unlock(&(pFileAccess->mutex));
}

/**
 * @brief: Initializes and returns pointer to FileAccess.
 * @param[in] maximum sum of process id currently active.
 */
FileAccessPtr fileAccess_init(long int max)
{
    FileAccessPtr fileAccess = (FileAccessPtr) malloc(sizeof(FileAccess));
    fileAccess->sum = 0;
    fileAccess->max = max;
    fileAccess->startAccess = startAccess;
    fileAccess->endAccess = endAccess;
    pthread_mutex_init(&(fileAccess->mutex), NULL);
    pthread_cond_init(&(fileAccess->waitForAccess), NULL);
    return fileAccess;
}


