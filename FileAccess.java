
/**
 * Created by surajdeuja on 11/1/15.
 */
public class FileAccess {
    private int max;
    private int sum;

    public FileAccess(int max) {
        this.max = max;
        this.sum = 0;
    }

    public synchronized void startAccess(int id) {
        while (sum + id > max) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        sum += id;
        notify();
    }

    public synchronized void endAccess(int id) {
        sum -= id;
        notifyAll();
    }
}
