#include <stdio.h>
#include <unistd.h>
#include "FileAccess.h"
#include <signal.h>
#include <stdlib.h>

static FileAccessPtr fileAccess;
static int numThreads;
static int max;
static int iterations;
static pthread_t *pthread_ids;
static pthread_mutex_t mutex;
static int id = 0;

void alarmHandle(int sig)
{
        printf("Process killed by signal\n");
        exit(EXIT_SUCCESS);
}

int getThreadId(void)
{
    int newId;
    /* lock the mutex so each thread id is unique */
    pthread_mutex_lock(&(mutex));
    newId = id + 1;
    id += 1;
    pthread_mutex_unlock(&(mutex)); /* mutex unlock */
    return newId;
}

void *threadMain(void *arg)
{
    int i;
    int rnd;
    int thread_id = getThreadId();
    fprintf(stdout, "Thread %d starting up %d\n", thread_id, iterations);
    for (i = 0; i < iterations; i++) {
        fileAccess->startAccess(fileAccess, thread_id);
        fprintf(stdout, "Thread %d starting file access\n", thread_id);
        rnd = rand() % 1000;
        usleep(rnd); /* File access and processing simulated by usleep */
        fprintf(stdout, "Thread %d ending file access\n", thread_id);
        fileAccess->endAccess(fileAccess, thread_id);
    }
    rnd = rand() % 1000;
    usleep(rnd);
    pthread_exit(0);
}

int main(int argc, char *argv[])
{
    int i;

    if (argc < 2) {
        fprintf(stderr, "Usage: %s <numThreads> <max sum> <iterations>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    numThreads = atoi(argv[1]);
    max = atoi(argv[2]);
    iterations = atoi(argv[3]);

    pthread_mutex_init(&mutex, NULL);
    fileAccess = fileAccess_init(max);
    pthread_ids = (pthread_t *) malloc(sizeof(pthread_t)*numThreads);

    /* Create threads to access file. */
    for (i = 0; i < numThreads; i++) {
        if (0 != pthread_create(&pthread_ids[i], NULL, threadMain, (void *)NULL))
        {
                fprintf(stdout, "Failed to create thread %d\n", i);
        }
    }

    signal(SIGALRM, alarmHandle);
    alarm(30);
    for (i = 0; i < numThreads; i++) {
        pthread_join(pthread_ids[i], NULL);
    }

    pthread_mutex_destroy(&mutex);
    free(pthread_ids);
    exit(EXIT_SUCCESS);
}
