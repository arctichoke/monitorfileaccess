CFLAGS=-g -Wall
LFLAGS=-lpthread

OBJS=FileAccess.o TestFileAccess.o
TARGET=TestFileAccess TestFileAccess.class 

all: $(TARGET)

TestFileAccess: FileAccess.h $(OBJS)
	$(CC) $(CFLAGS) -o $@ $(OBJS) $(LFLAGS)

%.class: %.java
	javac $<

clean:
	/bin/rm -f *.class *.o a.out $(OBJS) $(TARGET)

