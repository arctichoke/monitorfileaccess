import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by surajdeuja on 11/1/15.
 */
public class TestFileAccess {
    private int threadId = 0;

    static class FileAccessThread extends Thread {
        private FileAccess fileAccess;
        private int id;
        private int iterations;


        public FileAccessThread(FileAccess fileAccess, int id, int iterations) {
            this.fileAccess = fileAccess;
            this.id = id;
            this.iterations = iterations;
        }

        @Override
        public void run() {
            System.out.printf("Thread %d starting.\n", id);
            Random rnd = new Random();
            for (int i = 0; i < iterations; i++) {
                fileAccess.startAccess(id);
                System.out.printf("Thread %d starting file access.\n", id);
                try {
                    Thread.sleep(Math.abs(rnd.nextInt() % 1000));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.printf("Thread %d ending file access.\n", id);
                fileAccess.endAccess(id);
            }
            try {
                Thread.sleep(Math.abs(rnd.nextInt() % 1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized int getThreadId()
    {
        threadId++;
        return threadId;
    }

    public static void main(String args[]) {
        if (args.length < 3) {
            System.out.println("Usage: TestFileAccess <numThreads> <max> <numIteration>");
            System.exit(0);
        }
        int numThreads = Integer.parseInt(args[0]);
        int max = Integer.parseInt(args[1]);
        int iterations = Integer.parseInt(args[2]);

        ArrayList<FileAccessThread> fileAccessThreads = new ArrayList<FileAccessThread>();

        FileAccess fileAccessMonitor = new FileAccess(Integer.parseInt(args[1]));
        TestFileAccess testFileAccess = new TestFileAccess();

        for (int i = 0; i < numThreads; i++) {
            FileAccessThread fileAccessThread = new TestFileAccess.FileAccessThread(fileAccessMonitor, testFileAccess.getThreadId(), iterations);
            fileAccessThreads.add(fileAccessThread);
            fileAccessThread.start();
        }

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("Program killed by timer!");
                System.exit(0);
            }
        }, 20*1000);

        for (FileAccessThread fileAccessThread: fileAccessThreads) {
            try {
                fileAccessThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        timer.cancel(); // Cancel the timer and exits
    }
}
